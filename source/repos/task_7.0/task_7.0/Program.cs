﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace task_7._0
{
    class Program
    {
        static void Main(string[] args)
        {
            string letterEng = "a";

            string letterRus = "а";  

            using (StreamReader sr = new StreamReader(@"task_4.txt", Encoding.UTF8))
            {
                while (sr.Peek() >= 0)
                {
                    var arr = File.ReadAllText(@"task_4.txt", Encoding.UTF8);
                    
                    var doubles = arr.Split(new char[] { ';', ' ' });

                    for (int i = doubles.Length - 1; i >= 0; i--)
                    {
                        if (doubles[i].EndsWith(letterRus) || doubles[i].EndsWith(letterEng))
                        {
                            Console.WriteLine(doubles[i]);
                        }
                    }
                    Console.Read();
                }         
            }

            
        }
    }
}
