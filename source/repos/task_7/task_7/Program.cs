﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace task_7
{
    class Program
    {


        static void Main(string[] args)
        {
            string letterEng = "a";

            string letterRus = "а";

            var arr = File.ReadAllLines(@"task_4.txt", Encoding.UTF8);

            List<string> doubles = new List<string>();
            foreach (var item in arr)
            {
                doubles.AddRange(item.Split(new char[] { ';', ' ' }));
            }

            for (int i = doubles.Count - 1; i >= 0; i--)
            {
                if (doubles[i].EndsWith(letterRus) || doubles[i].EndsWith(letterEng))
                {
                    Console.WriteLine(doubles[i]);
                }
            }
            Console.Read();

        }
    }
}